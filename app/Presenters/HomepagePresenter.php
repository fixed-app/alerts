<?php

declare(strict_types=1);

namespace App\Presenters;

use App\Model\PushTokenModel;
use Nette;


final class HomepagePresenter extends Nette\Application\UI\Presenter
{

    /** @var PushTokenModel @inject */
    public $pushTokenModel;

    public function renderDefault()
    {
        $this->template->pushTokenCount = $this->pushTokenModel->getCount();

    }
}
