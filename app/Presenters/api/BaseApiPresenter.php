<?php

namespace App\Presenters\api;

use Nette\Application\Responses\JsonResponse;
use Nette\Application\UI\Presenter;
use Nette\Http\IResponse;
use Tracy\Debugger;

class BaseApiPresenter extends Presenter
{
    const SUCCESS_CODE = IResponse::S200_OK;
    const SUCCESS_CREATED = IResponse::S201_CREATED;
    const ERROR_CODE = IResponse::S400_BAD_REQUEST;
    const ERROR_NOT_FOUND = IResponse::S404_NOT_FOUND;
    const ERROR_NOT_ACCEPTABLE = IResponse::S406_NOT_ACCEPTABLE;
    const ERROR_FORBIEDEN = IResponse::S403_FORBIDDEN;
    const ERROR_UNAUTHORIZED = IResponse::S401_UNAUTHORIZED;
    const ERROR_INTERNAL_ERROR = IResponse::S500_INTERNAL_SERVER_ERROR;
    const ERROR_UNPROCESSABLE_ENTITY = IResponse::S422_UNPROCESSABLE_ENTITY;
    const ERROR_EXPIRED = IResponse::S423_LOCKED;

    protected $success_code = NULL;
    protected $error_code = NULL;
    protected $user = NULL;
    private $process = 1;
    private $successMessages = [];
    private $outResponse = [];
    private $method = null;
    private $respCode = null;
    public function startup(): void
    {
        $this->getHttpResponse()->setHeader('X-Powered-By', 'Fixed FW');


        $this->success_code = self::SUCCESS_CODE;
        $this->error_code = self::ERROR_CODE;

        parent::startup();

        Debugger::$showBar = FALSE;

        $method = strtolower($this->getHttpRequest()->getMethod()) . ucfirst(strtolower($this->getAction()));
        $this->method = strtolower($this->getHttpRequest()->getMethod());

        $this->process = 1;

        $rc = $this->getReflection();
        if (!$rc->hasMethod($method)) {
            $this->respCode = IResponse::S405_METHOD_NOT_ALLOWED;
            $this->outResponse['message'] = $this->t('Method not found');
        } else {
            $result = false;
            try {
                $result = $this->$method();

                if ($this->process == 0) {
                    $this->respCode = self::ERROR_INTERNAL_ERROR;
                } else {
                    if ($result === false) {
                        if ($this->respCode == $this->success_code) {
                            $this->respCode = $this->error_code;
                        }
                    } else {
                        $this->outResponse['message'] = count($this->successMessages) ? $this->successMessages : 'success';
                        $this->respCode = $this->success_code;

                        if (is_array($result)) {
                            $this->outResponse = $this->normalizeResult($result);
                        } else {

                        }
                    }
                }
            } catch (Exception $e) {
                $this->log_exception($e);
                $this->respCode = IResponse::S500_INTERNAL_SERVER_ERROR;
                $this->outResponse['message'] = $e->getMessage();
            }
        }

        if (!$this->respCode) {
            $this->respCode = self::SUCCESS_CODE;
        }

        if (substr($this->respCode, 0, 1) != 2 && (!array_key_exists('message', $this->outResponse) || $this->outResponse['message'] == "")) {
            $this->setResponseFailed(ApiErrors::UNKNOWN_ERROR);
        }

        $this->getHttpResponse()->setCode($this->respCode);
        $this->sendResponse(new JsonResponse($this->outResponse));
        $this->terminate();
    }

    private function normalizeResult($data)
    {
        foreach ($data as $id => &$value) {
            if (is_string($value)) {
                $value = trim($value);
            }
        }
        return $data;
    }
}
