<?php

namespace App\Model;

use Nette\Database\Connection;
use Nette\Database\Explorer;
use Nette\SmartObject;

class BaseModel
{
    use SmartObject;

    /**
     * @var Explorer
     */
    protected $database;
    protected $table = null;

    public function __construct(Explorer $database)
    {
        $this->database = $database;

        if ($this->table == null) {
            throw new Exception('Please set table name property protected $table');
        }
    }

    public function getDatabase(){
        return $this->database;
    }

    public function getCount(){
        return $this->getDatabase()->table($this->table)->count();
    }
}
